-----Command line version of Matermind game-----

1. This program generates 5 digit random number.
2. The player guesses the number. If the player guess the right number the he/she wins.
3. The player get 10 chances to guess correct number.
4. To run this game, save the program in location where you want to save.
5. open terminal and run python Mastermind_Game.py


-----Installation-----
1. Install python 3

------files-------
1. This repository contain 3 python file.
2. mastermind.py is the old version.
3. test_mastermind.py contains different test cases.
4. To play mastermind game, run Mastermind_Game.py