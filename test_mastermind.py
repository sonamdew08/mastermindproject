from mastermind_copy import Mastermind

def test_mastermind():
    answer = 10000
    testcase_1 = Mastermind(answer)
    
    guess_output = testcase_1.guess(10000)
   
    expected_output = {"is_correct": False, 'msg':'wrong input.!! Enter only 5 different digit'}

    assert guess_output == expected_output

    
    testcase_2 = Mastermind(32156)
    guess_output = testcase_2.guess(32184)
    
    expected_output = {"is_correct": False, "bulls": '3', "cows": '3'}
    assert guess_output == expected_output

    testcase_3 = Mastermind(99999)
    guess_output = testcase_3.guess(32184)
    expected_output = {"is_correct": False, "bulls": '0', "cows": '0'}
    assert guess_output == expected_output

    testcase_4 = Mastermind(53030)
    guess_output = testcase_4.guess(3218445667) # if user inputs number more than 5 digit
    expected_output = {"is_correct": False, 'msg':'wrong input.!! Enter only 5 different digit'}
    assert guess_output == expected_output

    testcase_4 = Mastermind(53030)
    guess_output = testcase_4.guess(22456) # if user inputs repeated number
    expected_output = {"is_correct": False, 'msg':'wrong input.!! Enter only 5 different digit'}
    assert guess_output == expected_output

    testcase_5 = Mastermind(53036)
    guess_output = testcase_5.guess(34) #if user inputs number less than five digit
    expected_output = {"is_correct": False, 'msg':'wrong input.!! Enter only 5 different digit'}
    assert guess_output == expected_output

    testcase_6 = Mastermind(53426)
    guess_output = testcase_5.guess(0) # if user inputs 0
    expected_output = {"is_correct": False, 'msg':'wrong input.!! Enter only 5 different digit'}
    assert guess_output == expected_output

    testcase_7 = Mastermind(53426)
    guess_output = testcase_5.guess("something")  # if user inputs string
    expected_output = {"is_correct": False, 'msg':'String not allowed'}
    assert guess_output == expected_output

test_mastermind()
