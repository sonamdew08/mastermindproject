import random


class Mastermind:

    def __init__(self, random_number):
        self.random_number = random_number

    def guess(self, guess_number):

        if type(guess_number) != int:
            return {
                    'is_correct': False,
                    'msg': "String not allowed"
                    }

        random_num = str(self.random_number)
        guess_number = str(guess_number)
        cows = 0
        bulls = 0
        if len(guess_number) != len(random_num) or len(set(guess_number)) != len(guess_number):
            return {
                    'is_correct': False,
                    'msg': 'wrong input.!! Enter only 5 different digit'
                    }

        if random_num != guess_number:
            for i in range(5):
                if guess_number[i] in random_num:
                    cows += 1
                if guess_number[i] is random_num[i]:
                    bulls += 1
            return {
                    'is_correct': False,
                    'bulls': str(bulls),
                    'cows': str(cows)
                    }
        return {
                'is_correct': True,
                'bulls': 5,
                'cows': 5
                }

    def reset(self):
        start = input("Do you want to play again?(y/n)")
        return start

def generate_random():
    while True:
        answer = random.randint(10000, 100000)
        if len(set(str(answer))) == 5:
            break
    return answer


def play():
    chance = 10
    while chance > 0:
        try:
            guess = int(input("\nEnter 5 digit number: "))
            result = Player.guess(guess)
            if len(result) is 2:
                print(result['msg'])
            else:
                if result['is_correct'] is True:
                    break
                chance -= 1
                print(result['bulls'] + " bulls, " + result['cows'] + " cows")
                print(str(chances) + " attempt left" )
        except:
            print("String not allowed")
    return chance

random_number = generate_random()
# print(random_number)
Player = Mastermind(random_number)


def check_mastermind():
    start = "y"
    while start is "y":
        if start is "y":

            chance = play()
            if chance is 0:
                print("\nYou lose")
                print("Number is " + str(random_number))
            else:
                print("\nYou win in " + str(11 - chance) + " attempt")
            start = Player.reset()


check_mastermind()
