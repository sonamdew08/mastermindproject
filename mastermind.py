import random
import os


class MasterMind:
    def __init__(self, answer):
        self.answer = answer

    def guess(self, number):
        random_num = str(self.answer)
        guess_number = str(number)
        count = 0
        bulls = 0
        if len(random_num) < len(guess_number):
            print("number exceeded.. ")
            return False
        if random_num != guess_number:
            for i in range(5):
                if guess_number[i] in random_num:
                    count += 1
                if guess_number[i] is random_num[i]:
                    bulls += 1
            # print(random_num)
            print(str(bulls) + "bulls, " + str(count) + "cows")
            return False
        return True
    
    
    def reset(self):
        check_MasterMind()
        start = input("\nDo you want to play?(y/n):")
        os.system('clear')
        return start
    
    

def check_MasterMind():
    chances = 10
    while chances > 0:
        try:
            guess_number = int(input("\nEnter 5 digit number:"))
            if len(str(guess_number)) != len(set(str(guess_number))):
                print("Enter only digit and all different")
            else:

                if player.guess(guess_number) is True:
                    print("successful")
                    break
                chances -= 1
                if chances is 0:
                    print("Failed!!\n\nThe answer is " + str(answer))
                else:
                    print(str(chances) + " attempt left!")
                    print("Try again")

        except IndexError:
            print("Wrong Input! Number is less in digit..")
        except:
            print("Wrong Input")

start='y'
answer = 0

while start is 'y':
    if start is 'y':
        if len(set(str(answer))) == 5:
            player = MasterMind(answer)
            start = player.reset()
        else:
            answer = random.randint(10000, 100000)
